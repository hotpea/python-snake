import pygame
import random
from pygame.locals import *
import winsound

pygame.init()

# pegar resolução atual
# infoObject = pygame.display.Info()
# grid_size = infoObject.current_h

grid_size = 600

pygame.init()


def create_border(border_color, screen_color):
    line_width = 10
    width = grid_size - 10
    height = grid_size - 10

    screen.fill(screen_color)
    # top line
    pygame.draw.rect(screen, border_color, [0, 0, width, line_width])
    # bottom line
    pygame.draw.rect(screen, border_color, [0, height, width, line_width])
    # left line
    pygame.draw.rect(screen, border_color, [0, 0, line_width, height])
    # right line
    pygame.draw.rect(screen, border_color, [width, 0, line_width, height + line_width])


def get_key_input(direction):
    if event.type == KEYDOWN:
        if event.key == K_UP and direction != DOWN:
            direction = UP
        if event.key == K_DOWN and direction != UP:
            direction = DOWN
        if event.key == K_RIGHT and direction != LEFT:
            direction = RIGHT
        if event.key == K_LEFT and direction != RIGHT:
            direction = LEFT
        if event.key == K_ESCAPE:
            die_sound()

    return direction


def on_grid_random():
    x = snake[0][0]
    y = snake[0][1]

    while (x, y) in snake:
        # retorna uma divisão inteira ( // )
        x = random.randint(20, grid_size - 30) // 10 * 10
        y = random.randint(20, grid_size - 30) // 10 * 10

    return x, y


def collision(c1, c2):
    return (c1[0] == c2[0]) and (c1[1] == c2[1])


def collision_sound():
    winsound.Beep(500, 50)


def die_sound():
    winsound.Beep(1000, 500)
    winsound.Beep(2500, 500)
    pygame.quit()


def create_snake(size, ini_position):
    snake_obj = [(ini_position, ini_position)]
    value = 1
    while value < size:
        snake_obj.append((ini_position + (value * 10), ini_position))
        value = value + 1

    return snake_obj


# macros
UP = 'up'
RIGHT = 'right'
DOWN = 'down'
LEFT = 'left'

# game settings
screen = pygame.display.set_mode((grid_size, grid_size), FULLSCREEN)
pygame.display.set_caption('Snake!!!!')

# game objetcs
snake = create_snake(5, grid_size//2)
snake_skin = pygame.Surface((10, 10))
snake_skin.fill((255, 255, 255))

apple_pos = on_grid_random()
apple = pygame.Surface((10, 10))
apple.fill((255, 0, 0))

my_direction = LEFT

clock = pygame.time.Clock()
speed = 10


while True:
    # limite para FPS
    clock.tick(speed)

    for event in pygame.event.get():
        if event.type == QUIT:
            die_sound()

        my_direction = get_key_input(my_direction)

    # colisão entre a snake e apple e adicionando nova posição a snake(crescer)
    if collision(snake[0], apple_pos):
        collision_sound()

        apple_pos = on_grid_random()
        snake.append((0, 0))

    # movimentar corpo da snake
    for i in range(len(snake) - 1, 0, -1):
        snake[i] = (snake[i - 1][0], snake[i - 1][1])

    for i in range(2, len(snake)):
        if snake[0] == snake[i]:
            # TODO: tela de game over/reiniciar game
            die_sound()

        if (snake[0][0] < 20 or snake[0][0] > grid_size - 30) or (snake[0][1] < 20 or snake[0][1] > grid_size - 30):
            # TODO: tela de game over/reiniciar game
            die_sound()

    if my_direction == UP:
        snake[0] = (snake[0][0], snake[0][1] - 10)
    if my_direction == DOWN:
        snake[0] = (snake[0][0], snake[0][1] + 10)
    if my_direction == RIGHT:
        snake[0] = (snake[0][0] + 10, snake[0][1])
    if my_direction == LEFT:
        snake[0] = (snake[0][0] - 10, snake[0][1])

    # limpa a tela a cada loop
    # screen.fill((220, 220, 220))
    create_border((255, 255, 255), (0, 0, 0))
    screen.blit(apple, apple_pos)

    # colocar snake na tela
    for pos in snake:
        # snake_skin poderia ser um sprite
        screen.blit(snake_skin, pos)

    pygame.display.update()
